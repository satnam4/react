import React from "react";

function TransactionTable({txns}) {
  let sortedTxns = [...txns];
  const sort = () => {
    sortedTxns.sort((a,b) => {
      if (a['amount'] < b['amount']) {
          return -1;
        }
        if (a['amount'] > b['amount']) {
          return 1;
        }
        return 0;
    });
    return sortedTxns;
  };

  return (
    <div className="layout-column align-items-center mt-50">
      <section className="layout-row align-items-center justify-content-center">
        <label className="mr-10">Transaction Date</label>
      </section>

      <div className="card mt-50">
          <table className="table">
              <thead>
              <tr className="table">
                  <th className="table-header">Date</th>
                  <th className="table-header">Description</th>
                  <th className="table-header">Type</th>
                  <th className="table-header">
                      <span id="amount" onClick={sort}>Amount ($)</span>
                  </th>
                  <th className="table-header">Available Balance</th>
              </tr>
              </thead>
              <tbody>

                {sortedTxns.map((value, index) => {
                  return <tr key={index}>
                              <td>{value.date}</td>
                              <td>{value.description}</td>
                              <td>{value.type === 1 ? "Test Debit" : "Test Credit"}</td>
                              <td>{value.amount}</td>
                              <td>{value.balance}</td>
                          </tr>
                })}
              </tbody>
          </table>
      </div>
    </div>
  );
}

export default TransactionTable;
